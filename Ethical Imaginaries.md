# Documentales/Feature films (película de formato largo)


    ***ongoing

### Una vita sul nostro pianeta (2020) IT
Uscito a settembre del 2020, questo film documentario sull’ambiente ha fatto presto parlare di sé, grazie al racconto del magistrale di David Attenborough. Il famoso divulgatore scientifico narra il cambiamento climatico attraverso il suo sguardo (Sir Attenborough ha più di 95 anni). Il documentario non è solo una fotografia dello stato attuale degli ecosistemi terrestri, ma anche un monito per il futuro. Sir Attenborough mette nelle mani delle prossime generazioni la sopravvivenza del nostro pianeta. Il film è un titolo imperdibile per chi cerca un racconto equilibrato e al contempo accorato da parte di un uomo che ha vissuto in prima persona i cambiamenti del nostro pianeta e desidera raccontarli apertamente.

### I am Greta (2020) IT
I am Greta – Una forza della natura, uscito nell’ottobre del 2020, è il documentario autorizzato sulla storia di Greta Thunberg, giovanissima attivista svedese. Il film di Nathan Grossman segue le vicende che vanno dalle prime proteste di fronte al parlamento svedese fino alla famosa traversata dell’Atlantico. Il documentario risulta particolarmente interessante perché le immagini ritraggono Greta Thunberg sia prima della fama, quando gli scioperi erano individuali e poco coperti dai media, sia durante l’anno in cui l’attivista si è mossa per tutto il mondo senza mai prendere un aereo. Grazie a I am Greta – Una forza della natura, si intuisce non solo la nascita del fenomeno mediatico associato a Greta Thunberg, ma viene ben rappresentato l’ inizio del movimento di attivisti che è nato su ispirazione delle sue parole e delle sue azioni.

### Ragazzi Irresponsabili (2020) IT

### David Attenborough: A life in Our Planet (2020) IT
Un crudo racconto dell'impatto dell'umanità sull'ambiente, con un messaggio ottimistico per le nuove generazioni. 

### Kiss the Ground (2020) IT
Scienziati ed attivisti famosi raccontano e mostrano quali potrebbero essere le soluzioni per un futuro sostenibile, a basso impatto ambientale.
    
### Brave Blue World: Racing to Solve our Water Crisis (2020) 
Tecnologie innovative per rendere l'acqua una risorsa sostenibile.

### Seaspiracy (2020)
Documentario che approfondisce il tema della pesca e del suo impatto a livello ambientale ed etico.

### [Our Planets](https://www.ourplanet.com/es/) (2019) EN/ES

Se divide en ocho capítulos de 50 minutos y hace referencia al impacto del ser humano sobre los animales, los hábitats y el planeta Tierra.

    Director: Alastair Fothergill
    Año: 2019
    Nacionalidad: Reino Unido
    Duración: 8 capítulos de 50 minutos

### Lemon (2019) ES
Con el formato de un cortometraje animado y producido por la Fundación Reina Sofía, busca concienciar sobre la presencia de plásticos en los océanos. La historia se centra en tres protagonistas: una tortuga, una gaviota y un polluelo. En el corto se plasma cómo los peligros de la contaminación irrumpen el ciclo de la naturaleza y pretende influenciar sobre la protección del medioambiente.  

    Director: Fundación Reina Sofía
    Año: 2019
    Nacionalidad: España
    Duración: 2 minutos 08 segundos

### El futuro del Ártico (2019) ES
Documental El Futuro Del Ártico
La transformación que está sufriendo el Ártico (área que se encuentra alrededor del Polo Norte) por el deshielo tendrá secuelas en el futuro: no sólo afectará a animales como los osos polares, sino que también se verá perjudicado el clima del planeta Tierra, desaparecerán muchas regiones y aumentará el nivel del mar. Sin embargo, este documental lo aborda desde otra perspectiva:  este hecho también puede ser positivo y que de él podrán surgir nuevos territorios y rutas marítimas. 

    Director: Deutsche Welle
    Año: 2019
    Nacionalidad: Alemania
    Duración: 42 minutos

### Ice on Fire /Hielo En Llamas (2019) EN/ES/
Producido y narrado por el actor Leonardo DiCaprio, este largometraje presenta distintas soluciones para disminuir y ralentizar la crisis ambiental. Muestra una perspectiva real de la actualidad del planeta y ofrece medidas innovadoras para no perder los sistemas de vida de la Tierra y concienciar sobre el peligro del carbono en la atmósfera. 

    Director: Leila Conners
    Año: 2019
    Nacionalidad: Estados Unidos
    Duración: 98 minutos

## PresaDiretta, Panni Sporchi (2019)
Puntata di PresaDiretta dedicata all'impatto ambientale del sistema moda.

### Down to Earth with Zac Efron (2019) (EN/ES/IT)
Serie TV che vede come protagonista Zac Efron in viaggio per il mondo mostrando esempi virtuosi di sostenibilità a livello industriale.

### Rotten (2019) 
Serie TV che svela i segreti dietro ad alimenti come l'avocado, lo zucchero, la cioccolata, etc.

### Refugiados climáticos (2018)
Uno de los problemas del cambio climático es que, en ocasiones, obliga a los habitantes del lugar afectado a migrar por catástrofes naturales como la sequía, inundaciones o huracanes. De hecho, el número de personas que se han visto obligadas a abandonar sus regiones y hogares lleva aumentando desde el año 1990. Un ejemplo son las altas temperaturas que está registrando el lago Chad, situado entre la frontera de Níger, Nigeria, Chad y Camerún. 

    Director: Deutsche Welle
    Año: 2018
    Nacionalidad: Alemania
    Duración: 42 minutos


### Diversidad en peligro (2018)
La extinción de las especies está directamente relacionada con la intervención humana en los distintos ecosistemas. 

    Director: Juan Antonio Rodríguez Llano y Ramón Campoamor
    Año: 2018
    Nacionalidad: España
    Duración: 50 minutos
    

### Antropocene (2018)

###  Domani (2018)

### Soyalism (2018)

### La fattoria dei nostri sogni (2018)

### Riverblue [2017)
Industria tessile e inquinamento dell'acqua: come sono correlati?

### Chasing Coral/En busca del coral (2017)
Muestra cómo las emisiones de carbono calientan el océano y provocan el ‘blanqueamiento del coral’; una muerte masiva del animal colonial que se está acelerando por toda el mundo. Las consecuencias de este fenómeno son incalculables, considerándose como una catástrofe ecológica.

    Director: Jeff Orlowski
    Año: 2017
    Nacionalidad: Estados Unidos
    Duración: 90 minutos

### The Milk System (2017) 
Quanto costa in termini ambientali l'industria dei latticini?


### Antes que sea tarde (2016)
National Geographic presenta este proyecto centrado en las consecuencias del cambio climático en diferentes rincones del mundo. Durante todo el documental se ofrecen varias perspectivas y debates acerca de cómo se puede solventar esta crisis que afecta a todos los continentes, para lo que cuenta con la colaboración de profesionales de la ciencia y de la política. Está indicado para los estudiantes de Secundaria.

    Director: Fisher Stevens
    Año: 2016
    Nacionalidad: Estados Unidos
    Duración: 95 minutos


### Freightened: El coste real del transporte maritimo (2016)
Esta película documental es una investigación sobre el transporte marítimo moderno y de sus consecuencias sobre el Medioambiente. Descubriremos una industria poderosa y poco regulada.
El 90% de todo lo que consumimos viene de ultramar con lo cual el transporte marítimo es una parte clave de nuestro modelo económico actual.
El documental indaga sobre cuál es el precio real del transporte marítimo y cual es su impacto sobre el medio ambiente y nuestras vidas en general.


### Years of living dangerously (2014 - 2016)
 Serie TV che racconta le catastrofi naturali dovute al cambiamento climatico.

### Alex James: Slowing Down Fast Fashion (2016)
Che impatto ha l'industria della moda sull'ambiente? L'ex bassista dei Blur ce lo racconta, intervistando designer e attivisti.

### Crudo paraíso (2016)
Surgió de la negativa de los ciudadanos canarios a que Repsol buscara hidrocarburos en aguas próximas a las Islas en el año 2012. Partiendo de aquel momento, el documental sitúa el debate en las energías: ¿hay alternativas? El proyecto, que cuenta con la participación de diversos ecologistas, investigadores e ingenieros medioambientales, apuesta por un modelo energético renovable y descentralizado para reemplazar el basado en los hidrocarburos.

    Director: Blanca Ordoñez de la Tena
    Año: 2016
    Nacionalidad: España
    Duración: 65 minutos

###  Minimalism: A documentary about the important things (2016) 
Come il minimalismo può rendere più sostenibile il nostro stile di vita. 

### A plastic Ocean (2016) 
Viaggio all'interno della plastica negli Oceani.
Il documentario di Craig Leeson approfondisce il drammatico impatto della plastica nei nostri oceani. Il regista australiano racconta le conseguenze devastanti che l’enorme quantità di plastica che riversiamo nei mari ha sull’intero ecosistema marino. Un esempio? Solo nel mar Mediterraneo, il rapporto tra plancton e particelle di plastica è di 2 a 1. Significa che ogni due particelle di plancton, nelle nostre acque, è presente una particella di plastica o microstaplastica: una proporzione spaventosa! Nell’oceano Pacifico esiste addirittura una isola interamente composta da plastica: si chiama Pacific Trash Vortex o Great Pacific Patch ed è grande quanto il Canada. Il primo passo verso una possibile soluzione? Eliminare la plastica usa e getta. Questo docufilm sull’ambiente è una visione obbligata per capire l’impatto che la plastica usa e getta ha sulla nostra vita e sul nostro futuro.

### Inside the Garbage of the World (Netflix 2016)
La verità sull'inquinamento dovuto alla plastica.

### Catching the Sun (2016)
En este documental queda claro que la energía solar no es solo parte de la solucion a los problemas del calentamiento global sino que también una oportunidad para reducir el desempleo.
Muestra ejemplos de varios emprendedores y activistas del sector solar, como las iniciativas llevadas a cabo en California que educan a la vez que crean empleo en su comunidad local, o ejemplos de paises como Alemania o China donde apuestan por la energía solar y están acelerando su producción de paneles solares.

### Antes Que Sea Tarde (2016)
National Geographic presenta este proyecto centrado en las consecuencias del cambio climático en diferentes rincones del mundo. Durante todo el documental se ofrecen varias perspectivas y debates acerca de cómo se puede solventar esta crisis que afecta a todos los continentes, para lo que cuenta con la colaboración de profesionales de la ciencia y de la política. Está indicado para los estudiantes de Secundaria.

    Director: Fisher Stevens
    Año: 2016
    Nacionalidad: Estados Unidos
    Duración: 95 minutos


### Punto di non ritorno - Before the Flood (2016)

Leonardo DiCaprio, documentario e ambiente: sembrano parole slegate? Invece no. Che l’attore premio Oscar sia anche un attivista ambientale non è un mistero. Già nel 2014 figurava tra i produttori di Cowspiracy. Nel 2016 va oltre e realizza un nuovo documentario (diretto da Fisher Stevens) in cui discute con alcune tra le persone più influenti della Terra riguardo il cambiamento climatico e i suoi effetti. Elon Musk, Barack Obama, Papa Francesco e John Kerry sono solo alcune delle personalità che appaiono in Before the Flood. Il documentario ha raccolto numerosi riconoscimenti e si configura come una vera e propria pietra miliare per gli interessati agli effetti del cambiamento climatico e alla sostenibilità ambientale.


  


### The True Cost (2015)    

### Primero la tierra (2015)
Portada Del Documental 'Primero La Tierra' Documentales Sobre El Cambio Climático
Muestra el estado actual de las construcciones que utilizan la tierra como materia prima en las distintas partes del mundo; un sistema que es barato, muy simple y con el que se ahorra en calefacción y aire acondicionado. El modelo también permite la autoconstrucción: crear viviendas a partir de tierra no contamina, ya que se utiliza un material natural. Esto es algo que saben perfectamente las tribus indígenas y comunidades que viven en armonía con la naturaleza y que participan en el documental con el objetivo de que estas enseñanzas se consoliden en todas las personas que habitan el planeta.

    Director: Carlos Brito Reyes
    Año: 2015
    Nacionalidad: Venezuela
    Duración: 86 minutos


### Cómo Cambiar El Mundo (2015)

Muestra cómo se originó el grupo ecologista Greenpeace.

    Director: Jerry Rothwell
    Año: 2015
    Nacionalidad: Reino Unido
    Duración: 110 minutos


### Unlearning (2015)

Plastic China (Prime video) 2016 - Documentario che parla del business del riciclo della plastica, in Cina.

### The True Cost [2015]
Documentario fondamentale per comprendere cosa si nasconde dietro al basso prezzo della fast fashion. 
 
### Mañana – Demain (2015)
Es un documental francés de carácter optimista que destaca varias iniciativas en diez países del mundo. Son ejemplos concretos que dan soluciones a los desafíos medioambientales y sociales del siglo XXI. El documental está distribuido en 5 temáticas : agricultura, energía, economía, educación y gobernanza. Cyril Dion y Mélanie Laurent, activistas relacionados con el mundo del cine, convierten su viaje por el mundo en una historia: buscan personas que tengan ideas innovadoras para hacer frente al cambio climático. Las ciudades en tránsito (Transition Towns) o el feminismo ecologista son algunos de los nuevos movimientos surgidos en Reino Unido e India, respectivamente, para acabar con él y que muestran aquí. Su principal objetivo es captar proyectos progresistas y vanguardistas para que los ciudadanos, las empresas y la clase política trate de llevarlos a cabo o encontrar otros nuevos.

    Director: Cyril Dion y Mélanie Laurent
    Año: 2015
    Nacionalidad: Francia
    Duración: 115 minutos


### Primero la tierra (2015)
Muestra el estado actual de las construcciones que utilizan la tierra como materia prima en las distintas partes del mundo; un sistema que es barato, muy simple y con el que se ahorra en calefacción y aire acondicionado. El modelo también permite la autoconstrucción: crear viviendas a partir de tierra no contamina, ya que se utiliza un material natural. Esto es algo que saben perfectamente las tribus indígenas y comunidades que viven en armonía con la naturaleza y que participan en el documental con el objetivo de que estas enseñanzas se consoliden en todas las personas que habitan el planeta.

    Director: Carlos Brito Reyes
    Año: 2015
    Nacionalidad: Venezuela
    Duración: 86 minutos




Virunga (2014)


12. Mission Blue (2014)
Conciencia sobre la problemática del plástico y la basura en los océanos. Esta producción se centra en la vida de la oceanógrafa y bióloga marina Sylvia Earle, quien ha luchado por detener la polución en los fondos marinos. Su testimonio detalla los problemas de los últimos años y los hechos que ha tenido que llevar a cabo para salvar al océano y su fauna de amenazas como los residuos tóxicos o la sobrepesca. 

    Director: Robert Nixon
    Año: 2014
    Nacionalidad: Estados Unidos
    Duración: 94 minutos


### Cowspiracy: El secreto de la sostenibilidad (2014)
Es un documental sobre el impacto de la explotación ganadera sobre el Medioambiente. Muestra que esta forma de obtención de alimento es uno de los problemas más importantes que tenemos actualmente en términos de sostenibilidad.

Explora también porque varias organizaciones medioambientales como Greenpeace, Rainforest Action Network y muchas otras no parecen tener interés en el problema. Al final nos ofrece varias soluciones hacia un mundo más sostenible con una creciente población.

### Chasing Ice (2014)
Nel 2005 il fotografo naturalista James Balog è andato in Artide per National Geographic per documentare gli effetti disastrosi del cambiamento climatico sui ghiacciai. 

### Un mondo in pericolo (2012)



### Trashed (Vimeo) 2012 
Jeremy Irons dà voce alla ricerca di verità che riguarda i rifiuti e il loro impatto sull'ambiente. 


### Crudo paraíso (2012)
Portada Del Documental 'Crudo Paraíso'

Surgió de la negativa de los ciudadanos canarios a que Repsol buscara hidrocarburos en aguas próximas a las Islas en el año 2012. 

    Director: Blanca Ordoñez de la Tena
    Año: 2016
    Nacionalidad: España
    Duración: 65 minutos



China Blue (Vimeo) 2011 - Documentario che racconta la produzione dei jeans in una industria Cinese.

### Into Eternity (2010)
Es un documental que trata sobre el almacenamiento en estratos geológicos a gran profundidad de la basura nuclear, los residuos producidos por las centrales nucleares en sus procesos de obtención de energía.

Normalmente esta basura se conserva en “depósitos temporales” porque se considera que las generaciones futuras podrán tratarlos. Pero estos depósitos pueden ser afectados con consecuencias catastróficas por desastres naturales o provocados por el hombre.

En Finlandia se ha decidido crear un depósito definitivo que durará todo el tiempo durante el cual el combustible será contaminante, es decir 100,000 años. Cuando el depósito esté lleno se sellará definitivamente para no abrirlo nunca.

Como crear un depósito de tales características, como hacerlo seguro y cómo informar a las generaciones futuras de lo que contiene el depósito son algunas de las preguntas a las cuales responde el documental.
Trailer en ingles

Versión completa subtitulada


## Waste Land (2010)
Registrato in circa 3 anni, questo documentario mostra le opere d'arte create con rifiuti della discarica, di Vik Muniz che da Brooklyn torna in Brasile, sua terra natale.

### Gasland (2010)

Es el documental ecológico clave que impulsó la lucha contra el fracking y la explotación de los gases de esquistos.

### Obsolescencia Programada (2010)

Comprar, tirar y volver a comprar. Este documental habla de los productos que se crean con fecha de caducidad; la conocida como obsolescencia programada: baterías que se estropean a los dos años, bombillas que se funden a las mil horas… Todo ello provoca un consumo cada vez mayor, lo que afecta a un planeta con recursos limitados. 

    Director: Cosima Dannoritzer
    Año: 2010
    Nacionalidad: España y Francia
    Duración: 75 minutos

### No Impact Man (2009)

El documental sigue al neoyorquino Colin Beavan y su familia durante un experimento de un año durante el cual intentan vivir sin tener ningún impacto sobre el medio ambiente.

Colin empieza su aventura porque está preocupado por el sobrecalentamiento global y se pregunta lo que puede hacer él como individuo. Desde ese momento elige vivir una vida sin impacto negativo sobre el medio ambiente. Por ejemplo, no utilizar transportes que funcionan con combustibles fósiles (metros, taxis, aviones), no consumir agua embotellada, no comprar comida rápida o no realizar nuevas compras junto a otras muchas más acciones que descubrirás al visionarla.

No Impact Man The Documentary por watchnewfilm

### FOOD, INC. (2008)

La producción comienza mostrando cómo la cría de animales está intervenida químicamente con hormonas, fertilizantes y otros productos que son nocivos para la salud.
    Director: Robert Kenner
    Año: 2008
    Nacionalidad: Estados Unidos
    Duración: 94 minutos


### La historia de las cosas (2007)
Todos los objetos que hay en nuestras vidas generan un impacto medioambiental desde su extracción hasta su venta, uso y disposición. Enseña y divierte a partes iguales, por lo que está especialmente recomendado para niños.

    Director: Louis Fox
    Año: 2007
    Nacionalidad: Estados Unidos
    Duración: 20 minutos
    


### La huella ecológica del hombre (2007)
National Geographic muestra en esta cinta el impacto ambiental producido por el hombre. Así, pone en relieve el exceso de recursos que explotan las personas durante toda  su vida, sobre todo, en las ciudades. Todo este consumo irracional y las consecuencias de los residuos que generan se califican como ‘huella ecológica del hombre’.

    Director: Nick Watts
    Año: 2007
    Nacionalidad: Estados Unidos
    Duración: 47 minutos


### Una scomoda verità (2007) e Una scomoda verità 2 (2017)
   


### Una verdad incomoda – An inconvenient truth (2006)

Al Gore (antiguo vicepresidente de los Estados Unidos) nos explica de manera clara que el cambio climático es real, de origen humano y que va a producir cambios cataclísmicos si no actuamos ya.
Nos presenta la información a veces de manera humorística, a menudo de manera emocional y siempre con un efecto fascinante. Al final es una película que nos inspira a actuar.

Una Verdad Incómoda Al Gore-Español from JVasquezSierra on Vimeo.

    Director: Davis Guggenheim
    Año: 2006
    Nacionalidad: Estados Unidos
    Duración: 90 minutos


Documentales ecológicos cortos

[El Camino del sol](https://www.youtube.com/watch?v=aOxc8KOOzEM&feature=youtu.be)
Es un documental de la ANPIER (Asociacion Nacional de Productores de Energía Fotovoltaica). La película sigue al actor Ismael Fritschi participando en el Camino del Sol (un evento organizado por la ANPIER) donde participaron miles de personas.
Cuenta de manera genérica la historia de miles de familias españolas que habían invertido en instalaciones de energía solar fotovoltaica animadas por el estado Español y que se vieron muy afectadas por unos recortes, de hasta el 50%, respecto a la tarifa prevista en el Boletín Oficial del Estado. De hecho algunos habían invertido todos sus ahorros e incluso hipotecado sus bienes.

###Connect4Climate 
organizó un concurso para dar la oportunidad a jóvenes (de entre 18 y 35 años) de contar su visión acerca del cambio climático. Presentamos abajo las mejores películas premiadas.

#### Three seconds
https://www.youtube.com/watch?v=sacc_x-XB1Y

El personaje principal es un artista, poeta y rapero llamado Prince Ea. Conocido por haber hecho algunos videos virales como “Dear Future generations: Sorry” (Queridas generaciones futuras: Disculpad) o “Can we Auto-Correct Humanity?” (Podemos autocorregir la humanidad?).
En este documental de formato corto Prince Ea está sentado en la playa explicando que si condensas la existencia de la Tierra en 24 horas el ser humano ha existido sólo 3 segundos. Luego explica el impacto que hemos tenido en el planeta y cómo podemos asegurarnos de llegar al cuarto segundo.

 
#### “The snow guardian” (El guardián de la nieve)

Este documental en formato corto nos cuenta la historia de Billy Barr: un hombre que vivió solo durante 40 años en una pequeña cabaña con autoconsumo solar en una de la partes más nubladas de los Estados Unidos, en Gunnison, Colorado. El empezó a recolectar datos sobre los niveles de nieve sin un objetivo real, sólo como un pasatiempo.
Cuando investigadores del cambio climático encontraron la investigación de Billy les aportó una información vital que representaba un claro testimonio del calentamiento global. Al final del corto Barr nos da algunos consejos para el futuro basados en su propia experiencia de cómo sobrevivir en la montaña.

 
#### “Love Note to an island” (Carta de amor para una isla)

Este corto nos habla de Kiribati, una república del Pacífico Central compuesta de varias islas que están aproximadamente a dos metros por encima del nivel del mar. Los científicos han previsto que la república no tiene más de 30 años de vida antes de quedar sumergida. El documental nos muestra cómo los habitantes se adaptan y luchan contra el cambio climático.
Este corto se convertirá en un documental de formato largo llamado Millenium Island después de haber conseguido financiarse a través de una campaña de crowdfunding con Kickstarter.

____

Sources

https://www.ecrowdinvest.com/blog/documentales-ecologicos/

https://www.educaciontrespuntocero.com/recursos/documentales-ecologia-y-cambio-climatico/
