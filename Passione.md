    *ongoing


### Pathos

## Passion in Spinoza

## Activism as passion

Activism is often flawed; besides passion and dedication, it requires skills, knowledge and resources to achieve systemic change goals.

The risk is to fight against current exploitative development models, in a form that is ineffective in the long term.  We pursue battles in a piecemeal way, dissipate energies and end up practising 'blah blah blah' (like the policy-makers to whom we are referred).

Battles have a low impact in terms of change and therefore we feel disappointed or demotivated.

In order to generate consistent change, activism needs to be armed with diverse skills, hard work and good organisation. 

We need to build a solid professional capacity structure: open our horizons, understand our role in the community and what goals we want to achieve.



