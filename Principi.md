    *Ongoing

The purpose of following meta-principles is to serve as the loose basis for more specifics code of conduct, to be declined according to particular grounds

### Notes on principles

* Deconstructing is a preparatory introduction for constructive purposes based on research for a coherent critique of reality.


* Human beings inhabit language; healthy communication could be patiently achieved by diving language structures.


* Participation is respecting others and building narratives together understanding differences and deepening affinities.


* For an effective and ethical coexistence, the direction should be kept outwards and not towards ourselves.


* Conflicts are inevitable; their management involves the strength to see one's mistakes and make amends.


* Knowledge is a process made up of reliable sources and appropriate analysis of data collected.


