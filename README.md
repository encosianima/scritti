    *Ongoing

### Scritti

(Prolegomena to an *Ethics demonstrated with free code*)



[Who am I?](https://www.valentinamesseri.eu)


I acquired a humanities academic background parallel to down on-earth experience in Activism, this practice meant both to embody and ethnographically research Ethics and Values.
