    *Ongoing


## Notes

*Memory is sometimes a trap, the true story is far more objective* 

Historian Yuval Noah Harari, argues that liberal humanism has shifted from the *individual* as the source of truth to *feelings* as the centre of all truth. When individual feelings take precedence over other forms of knowledge - expertise, science, rationality and religious faith - *'my truth' becomes the source of knowledge* and all issues must be decided through the lens of relativism and subjectivity.

Feeling is knowledge moving directly, empathically and different from critical thinking, wich is also a process of knowledge.

S. Pinkers book on Rationality's [review](https://www.psychologytoday.com/intl/blog/hot-thought/202110/steven-pinker-rationality).
Why a book on rationality now? With the world's steady progress towards verifiable rationality since the Enlightenment, we now resort to our emotions and feelings rather than our ability to reason. Using Google to analyse language use from 1850 to 2019, the researchers found that the use of words associated with rationality such as 'conclusion' and 'determine' increased after 1850, while emotionally charged terms such as 'believe' and 'feel' decreased. This trend existed both in fiction and non-fiction books and in the popular press. Since the 1980s and our so-called 'post-truth era', usage has started to change. Emotionally charged words have increased, in parallel with the *shift from collectivist to individualist language and from rationality to feeling*. <sup>[1](#myfootnote1)</sup>

Notes:

(<a name="myfootnote1">1</a>): https://www.pnas.org/content/118/51/e2107848118
